---
title: SpringBoot application yml Sample
subtitle: YML file structure
date: 2017-03-21
tags: ["example", "code", "SpringBoot"]
---

Here's a yml code chunk with syntax highlighting:


```yml
#sensible defaults.
#These should work for local execution so that developers and automated tests can run the app without special profiles or configuration.
spring:
    application:
        #set this to the name of your application. It will also be used as your context path.
        name: my-api
        cayenne_datasource:
            psa:
                username: user-name 
                password: Ipickedit
                url: jdbc:oracle:thin:@xdev01-scan.server.org:1521/APP.SERVER.ORG
                driver: oracle.jdbc.driver.OracleDriver    
    profiles:
        default: discovery,localdev
        
server:
  port: 38181
management:
  port: 39191
        
tomcat:
  jvmroute: 35478
  ajp:
    port: 34512
    redirectPort: 31236     
    
    
logging:
    level:
        net.emecas.boot.api: DEBUG            
        
#set credentials for the api and expose them via eureka metadata
api:
    security:
        user: api-user
        password: api-pwd
```
